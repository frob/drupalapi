<?php
/**
 * @file
 * Install profile for DrupalAPI.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drupalapi_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = t('DrupalAPI');
}

/**
 * Implements hook_install_tasks().
 */
function drupalapi_install_tasks() {
  $tasks = array();

  $tasks['drupalapi_api_setup'] = array(
    'display_name' => st('Index API'),
    'type' => 'batch',
    'run' => php_sapi_name() !== 'cli' ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
  );

  return $tasks;
}

/**
 * DrupaAPI API setup installation task callback.
 */
function drupalapi_api_setup() {
  // Ensure that projects and branches only get automatically setup on install.
  $branches = api_get_branches();
  if (empty($branches)) {
    // Setup API project.
    $project = new stdClass();
    $project->project_name = 'drupal';
    $project->project_title = 'Drupal';
    $project->project_type = 'core';
    api_save_project($project);

    // Setup API branches.
    $versions = array(6, 7);
    foreach ($versions as $version) {
      $branch = new stdClass();
      $branch->branch_name = $version;
      $branch->core_compatibility = "{$version}.x";
      $branch->preferred = $version == 7;
      $branch->project = 'drupal';
      $branch->data = array(
        'directories' =>
          realpath("./profiles/drupalapi/libraries/drupal-{$version}.x") . "\n" .
          realpath("./profiles/drupalapi/libraries/documentation-{$version}.x/developer"),
      );
      $branch->title = "Drupal {$version}";
      api_save_branch($branch);
    }
  }

  // Return Batch array.
  return array(
    'title' => t('Indexing DrupalAPI reference'),
    'operations' => array(
      array('drupalapi_api_batch', array())
    ),
    'finished' => 'drupalapi_api_batch_finished',
  );
}

/**
 * Operation callback for DrupalAPI Indexing batch.
 */
function drupalapi_api_batch(&$context) {
  // Try to increase the maximum execution time if it is too low.
  if (ini_get('max_execution_time') < 240 && !ini_get('safe_mode')) {
    set_time_limit(240);
  }

  // Initialize batch.
  if (!isset($context['sandbox']['progress'])) {
    // Update all branches.
    module_load_include('inc', 'api', 'parser');
    api_update_all_branches();

    // Run API update queue items.
    $context['sandbox']['api_cron_queue_info'] = api_cron_queue_info();
    $function = $context['sandbox']['api_cron_queue_info']['api_branch_update']['worker callback'];
    $queue = DrupalQueue::get('api_branch_update');
    while ($item = $queue->claimItem()) {
      $function($item->data);
      $queue->deleteItem($item);
    }

    // Determine how many files need parsing.
    $queue = DrupalQueue::get('api_parse');
    $context['sandbox']['max'] = $queue->numberOfItems();
    $context['sandbox']['progress'] = 0;
  }

  // Process abtch.
  else {
    // Run 'API Parse' queue items.
    $function = $context['sandbox']['api_cron_queue_info']['api_parse']['worker callback'];
    $end = time() + (isset($info['api_parse']['time']) ? $info['api_parse']['time'] : 15);
    $queue = DrupalQueue::get('api_parse');
    while (time() < $end && ($item = $queue->claimItem())) {
      $function($item->data);
      $queue->deleteItem($item);
    }
  }

  // Determine how many files still need parsing.
  $queue = DrupalQueue::get('api_parse');
  $context['sandbox']['progress'] = $context['sandbox']['max'] - $queue->numberOfItems();

  // Provide status and progress information.
  $function = function_exists('st') ? 'st' : 't';
  $context['message'] = $function('!progress of !max files indexed.', array(
    '!progress' => $context['sandbox']['progress'],
    '!max' => $context['sandbox']['max'],
  ));
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Finished callback for DrupalAPI Indexing batch.
 */
function drupalapi_api_batch_finished($success, $results) {
  variable_set('drupalapi_indexed', TRUE);
}
