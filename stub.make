api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.19

projects[drupalapi][download][type] = git
projects[drupalapi][download][url] = http://git.drupal.org/project/drupalapi.git
projects[drupalapi][download][branch] = 7.x-1.x
projects[drupalapi][type] = profile
