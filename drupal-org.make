core = 7.x
api = 2



; Modules
projects[api][subdir] = contrib
projects[api][version] = 1.2

projects[context][subdir] = contrib
projects[context][version] = 3.0-beta6

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.2

projects[features][subdir] = contrib
projects[features][version] = 2.0-beta1

projects[grammar_parser_lib][subdir] = contrib
projects[grammar_parser_lib][version] = 1.1
; Remove makefiles - http://drupal.org/node/1463770#comment-6788838
projects[grammar_parser_lib][patch][] = http://drupal.org/files/no_make-1463770-13.patch

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.0

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0



; Libraries
libraries[documentation-6.x][download][type] = git
libraries[documentation-6.x][download][url] = http://git.drupal.org/project/documentation.git
libraries[documentation-6.x][download][branch] = 6.x-1.x

libraries[documentation-7.x][download][type] = git
libraries[documentation-7.x][download][url] = http://git.drupal.org/project/documentation.git
libraries[documentation-7.x][download][branch] = 7.x-1.x

libraries[drupal-6.x][download][type] = git
libraries[drupal-6.x][download][url] = http://git.drupal.org/project/drupal.git
libraries[drupal-6.x][download][branch] = 6.x

libraries[drupal-7.x][download][type] = git
libraries[drupal-7.x][download][url] = http://git.drupal.org/project/drupal.git
libraries[drupal-7.x][download][branch] = 7.x

libraries[grammar_parser][download][type] = git
libraries[grammar_parser][download][url] = http://git.drupal.org/project/grammar_parser.git
libraries[grammar_parser][download][revision] = ca560ae7877f90ac71d8cae54cb4a13a16c260f6
